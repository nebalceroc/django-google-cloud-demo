from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse
from django.views import View
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework import viewsets, mixins
from rest_framework.response import Response
from rest_framework.decorators import authentication_classes, permission_classes, api_view, action
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
from invoice.serializers import *
from invoice.permissions import *
from invoice.services import *
from invoice.forms import *
from invoice.models import *
from invoice.mixins import *

from easy_pdf.views import PDFTemplateView

class IndexView(View):
    def get(self, request):
        return redirect('/main/dashboard/')

class MainView(LoginRequiredMixin, View):
    login_url = '/accounts/login/'
    redirect_field_name = 'next'
    def get(self, request, module):
        token_service = TokenService()
        context = {'token': token_service.get_user_token(request.user)}
        if module == 'dashboard':
            return render(request, 'dashboard.html', context)
        elif module == 'clients':
            return render(request, 'clients.html', context)
        elif module == 'items':
            return render(request, 'items.html', context)
        elif module == 'invoices':
            return render(request, 'invoices.html', context)

class FormsView(LoginRequiredMixin, View):
    login_url = '/accounts/login/'
    redirect_field_name = 'next'
    def get(self, request, form):
        if form == 'client':
            form = ClientForm()
            form_id = 'modal-form'
            form_button_id = 'modal-submit-btn'
        if form == 'invoice':
            form = InvoiceForm(user=UserProfile.objects.get(user=request.user))
            form_id = 'modal-form'
            form_button_id = 'modal-submit-btn'
        if form == 'invoice_item':
            form = InvoiceItemForm()
            form_id = 'invoice-item-form'
            form_button_id = 'invoice-item-btn'
        if form == 'profile':
            form = UserProfileForm(user=request.user.id)
            form_id = 'user-profile-form'
            form_button_id = 'user-profile-btn'

        return render(request, 'form.html', {'form': form, 'form_id': form_id, 'form_button_id': form_button_id})

class CreateView(LoginRequiredMixin, View):
    login_url = '/accounts/login/'
    redirect_field_name = 'next'
    def get(self, request):
        token_service = TokenService()
        context = {'token': token_service.get_user_token(request.user)}
        return render(request, 'create.html', context)

class EditView(LoginRequiredMixin, View):
    login_url = '/accounts/login/'
    redirect_field_name = 'next'
    def get(self, request, id):
        token_service = TokenService()
        context = {'token': token_service.get_user_token(request.user), 'invoice_id': id}
        return render(request, 'create.html', context)

class InvoiceView(LoginRequiredMixin, View):
    login_url = '/accounts/login/'
    redirect_field_name = 'next'
    def get(self, request, id):
        token_service = TokenService()
        context = {'token': token_service.get_user_token(request.user), 'invoice_id': id}
        return render(request, 'invoice.html', context)

class PdfView(LoginRequiredMixin, PDFTemplateView):
    template_name = 'pdf.html'
    base_url = 'file://' + settings.STATIC_ROOT
    download_filename = 'invoice.pdf'
    def get_context_data(self, **kwargs):
        context = super(PdfView, self).get_context_data(
            title='Invoice',
            **kwargs
        )
        invoice = Invoice.objects.get(pk=int(kwargs.get('invoice_id',None)))
        items = InvoiceItem.objects.filter(invoice=invoice)
        context['invoice'] = invoice
        context['items'] = items
        return context

@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated, UserOwnerPermissions))
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated, UserProfileOwnerPermissions))
class UserProfileViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows UserProfile to be viewed or edited.
    """
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

@authentication_classes((TokenAuthentication,))
@permission_classes((ClientOwnerPermissions,))
class ClientViewSet(CreateClientMixin,
                     mixins.RetrieveModelMixin,
                     UpdateClientMixin,
                     mixins.DestroyModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    """
    API endpoint that allows Client to be viewed or edited.
    """
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

    @action(detail=False)
    def user_clients(self, request):
        owner_profile = UserProfile.objects.get(user=request.user)
        user_clients = Client.objects.filter(owner=owner_profile)
        serializer = self.get_serializer(user_clients, many=True)
        return Response(serializer.data)

@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated,))
class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows Item to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated, InvoiceOwnerPermissions))
class CreateInvoiceViewSet(CreateInvoiceMixin,
                     mixins.RetrieveModelMixin,
                     UpdateInvoiceMixin,
                     mixins.DestroyModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    """
    API endpoint that allows Invoice to be viewed or edited.
    """
    queryset = Invoice.objects.all()
    serializer_class = CreateInvoiceSerializer

@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated, InvoiceOwnerPermissions))
class InvoiceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows InvoiceItem to be viewed or edited.
    """
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer

    @action(detail=False)
    def recent_invoices(self, request):
        owner_profile = UserProfile.objects.get(user=request.user)
        recent_invoces = Invoice.objects.filter(owner=owner_profile).order_by('-issue_date')[:5]
        serializer = self.get_serializer(recent_invoces, many=True)
        return Response(serializer.data)


    @action(detail=False)
    def user_invoices(self, request):
        owner_profile = UserProfile.objects.get(user=request.user)
        user_invoices = Invoice.objects.filter(owner=owner_profile)
        serializer = self.get_serializer(user_invoices, many=True)
        return Response(serializer.data)

@authentication_classes((TokenAuthentication,))
@permission_classes((IsAuthenticated, InvoiceItemOwnerPermissions))
class InvoiceItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows InvoiceItem to be viewed or edited.
    """
    queryset = InvoiceItem.objects.all()
    serializer_class = InvoiceItemSerializer
