from django.contrib.auth.models import User
from rest_framework import serializers
from invoice.models import UserProfile, Client, Item, Invoice, InvoiceItem


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email']

class UserProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    class Meta:
        model = UserProfile
        fields = ['user', 'phone', 'address1', 'address2' ,'zipcode']
        read_only_fields = ['user']

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'owner', 'phone', 'first_name', 'last_name', 'address1', 'address2' ,'zipcode']

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ['id', 'name', 'description', 'price']

class InvoiceItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = InvoiceItem
        fields = ['id', 'item', 'amount', 'qty_rate']
        depth = 2

class InvoiceItemCreateSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
        model = InvoiceItem
        fields = ['id', 'item', 'amount', 'qty_rate']

class CreateInvoiceSerializer(serializers.ModelSerializer):
    items = InvoiceItemCreateSerializer(many=True)
    class Meta:
        model = Invoice
        fields = ['id', 'owner', 'client', 'subtotal', 'tax', 'total', 'issue_date', 'terms', 'items']
        read_only_fields = ['id']
    def create(self, validated_data):
        items_data = validated_data.pop('items')
        invoice = Invoice.objects.create(**validated_data)
        for item_data in items_data:
            item_data.pop('id')
            InvoiceItem.objects.create(invoice=invoice, **item_data)
        return invoice
    def update(self, instance, validated_data):
        instance.client = validated_data.get('client', instance.client)
        instance.subtotal = validated_data.get('subtotal', instance.subtotal)
        instance.tax = validated_data.get('tax', instance.tax)
        instance.total = validated_data.get('total', instance.total)
        instance.terms = validated_data.get('terms', instance.terms)
        items_data = validated_data.pop('items')
        instance.save()
        for item_data in items_data:
            print(item_data)
            if item_data.get('id', None) == -1:
                item_data.pop('id')
                InvoiceItem.objects.create(invoice=instance, **item_data)
        return instance

class InvoiceSerializer(serializers.ModelSerializer):
    items = serializers.SerializerMethodField()
    class Meta:
        model = Invoice
        fields = ['id', 'owner', 'client', 'subtotal', 'tax', 'total', 'issue_date', 'terms', 'items']
        read_only_fields = ['id']
        depth = 2

    def get_items(self, obj):
        return InvoiceItemSerializer(InvoiceItem.objects.filter(invoice=obj), many=True).data
