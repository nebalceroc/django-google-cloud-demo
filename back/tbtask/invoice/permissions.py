from rest_framework import permissions

class ClientOwnerPermissions(permissions.BasePermission):
    """
    Handles permissions for clients.  The basic rules are

     - owner may GET, PUT, POST, DELETE
     - auth users can create
     - auth owners can patch
    """

    def has_object_permission(self, request, view, client):
        return request.user == client.owner.user

class InvoiceOwnerPermissions(permissions.BasePermission):
    """
    Handles permissions for invoices.  The basic rules are

     - owner may GET, PUT, POST, DELETE
     - auth users can create
     - auth owners can patch
    """

    def has_object_permission(self, request, view, invoice):
        if view.action == 'create':
            return True
        else:
            # check if user is owner
            return request.user == invoice.owner.user

class InvoiceItemOwnerPermissions(permissions.BasePermission):
    """
    Handles permissions for invoice items.  The basic rules are

     - owner may GET, PUT, POST, DELETE
     - auth users can create
     - auth owners can patch
    """

    def has_object_permission(self, request, view, invoice_item):
        if view.action == 'create':
            return True
        else:
            # check if user is owner
            return request.user == invoice_item.invoice.owner.user

class UserProfileOwnerPermissions(permissions.BasePermission):
    """
    Handles permissions for user profiles.  The basic rules are

     - owner may GET, PUT, POST, DELETE
     - auth users can create
     - auth owners can patch
    """

    def has_object_permission(self, request, view, profile):
        if view.action == 'create':
            return True
        else:
            # check if user is owner
            return request.user == profile.user

class UserOwnerPermissions(permissions.BasePermission):
    """
    Handles permissions for user profiles.  The basic rules are

     - owner may GET, PUT, POST, DELETE
     - auth users can create
     - auth owners can patch
    """

    def has_object_permission(self, request, view, user):
        if view.action == 'create':
            return True
        else:
            # check if user is owner
            return request.user == user
