import datetime

from django.db import models
from django.contrib.auth.models import User

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=15, default='')
    address1 = models.CharField(max_length=100, default='')
    address2 = models.CharField(max_length=100, default='')
    zipcode = models.CharField(max_length=32, default='')

    def __str__(self):
        return '{} {}'.format(self.user.first_name, self.user.last_name)

class Client(models.Model):
    owner = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    phone = models.CharField(max_length=15)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address1 = models.CharField(max_length=100)
    address2 = models.CharField(max_length=100)
    zipcode = models.CharField(max_length=32)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

class Item(models.Model):
    name = models.CharField(max_length=50)
    price = models.FloatField(default=0)
    description = models.TextField()

    def __str__(self):
        return '{} ${} u'.format(self.name, self.price)

class Invoice(models.Model):
    owner = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    subtotal = models.FloatField(default=0)
    tax = models.FloatField(default=0)
    total = models.FloatField(default=0)
    issue_date = models.DateField(default=datetime.date.today)
    terms = models.TextField()

class InvoiceItem(models.Model):
    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    qty_rate = models.IntegerField(default=0)
