from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

class TokenService():
    def get_user_token(self, user):
        return Token.objects.get(user=user)
