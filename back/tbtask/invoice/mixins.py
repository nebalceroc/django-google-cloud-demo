import json
from rest_framework import status
from rest_framework.response import Response
from rest_framework.settings import api_settings
from invoice.serializers import *

class CreateInvoiceMixin:
    """
    Create a invoice instance.
    """
    def create(self, request, *args, **kwargs):
        custom_data = {
            'owner': UserProfile.objects.get(user=request.user).pk,
            'client': request.data.get('client', 0),
            'subtotal': request.data.get('subtotal', 0),
            'tax': request.data.get('tax', 0),
            'total': request.data.get('total', 0),
            'terms': request.data.get('terms', ''),
            'items': json.loads(request.data.get('items', ''))
        }
        serializer = self.get_serializer(data=custom_data)
        serializer.is_valid(raise_exception=True)
        serializer = InvoiceSerializer(self.perform_create(serializer))
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}

class UpdateInvoiceMixin:
    """
    Update a invoice instance.
    """
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        custom_data = {
            'owner': UserProfile.objects.get(user=request.user).pk,
            'client': request.data.get('client', 0),
            'subtotal': request.data.get('subtotal', 0),
            'tax': request.data.get('tax', 0),
            'total': request.data.get('total', 0),
            'terms': request.data.get('terms', ''),
            'items': json.loads(request.data.get('items', ''))
        }
        serializer = self.get_serializer(instance, data=custom_data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer = InvoiceSerializer(self.perform_update(serializer))
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)

class CreateClientMixin:
    """
    Create a client instance.
    """
    def create(self, request, *args, **kwargs):
        custom_data = {
            'owner': UserProfile.objects.get(user=request.user).pk,
            'phone': request.data.get('phone', ''),
            'first_name': request.data.get('first_name', ''),
            'last_name': request.data.get('last_name', ''),
            'address1': request.data.get('address1', ''),
            'address2': request.data.get('address2', ''),
            'zipcode': request.data.get('zipcode', ''),
        }
        serializer = self.get_serializer(data=custom_data)
        serializer.is_valid(raise_exception=True)
        serializer = ClientSerializer(self.perform_create(serializer))
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}

class UpdateClientMixin:
    """
    Update a client instance.
    """
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        custom_data = {
            'owner': UserProfile.objects.get(user=request.user).pk,
            'phone': request.data.get('phone', ''),
            'first_name': request.data.get('first_name', ''),
            'last_name': request.data.get('last_name', ''),
            'address1': request.data.get('address1', ''),
            'address2': request.data.get('address2', ''),
            'zipcode': request.data.get('zipcode', ''),
        }
        serializer = self.get_serializer(instance, data=custom_data, partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer = ClientSerializer(self.perform_update(serializer))
        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)

    def perform_update(self, serializer):
        serializer.save()

    def partial_update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        return self.update(request, *args, **kwargs)
