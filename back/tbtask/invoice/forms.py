from django import forms
from invoice.models import *

class UserProfileForm(forms.Form):
    def __init__(self, *args, **kwargs):
        user_id = kwargs.pop('user')
        super(UserProfileForm, self).__init__(*args, **kwargs)
        user = User.objects.get(pk=user_id)
        profile = UserProfile.objects.get_or_create(user=user)[0]
        self.fields['id'] = forms.CharField(label='ID', max_length=50, disabled=True, initial=profile.id, widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['first_name'] = forms.CharField(label='First name', max_length=50, initial=user.first_name, widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['last_name'] = forms.CharField(label='Last name', max_length=50, initial=user.last_name, widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['email'] = forms.EmailField(label='Email', max_length=50, initial=user.email, widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['phone'] = forms.CharField(label='Phone', max_length=15, initial=profile.phone, widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['address1'] = forms.CharField(label='Address 1', max_length=100, initial=profile.address1, widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['address2'] = forms.CharField(label='Address 2', max_length=100, initial=profile.address2, widget=forms.TextInput(attrs={'class' : 'form-control'}))
        self.fields['zipcode'] = forms.CharField(label='zipcode', max_length=32, initial=profile.zipcode, widget=forms.TextInput(attrs={'class' : 'form-control'}))


class ClientForm(forms.Form):
    first_name = forms.CharField(label='First name', max_length=50, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    last_name = forms.CharField(label='Last name', max_length=50, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    phone = forms.CharField(label='Phone', max_length=15, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    address1 = forms.CharField(label='Address 1', max_length=100, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    address2 = forms.CharField(label='Address 2', max_length=100, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    zipcode = forms.CharField(label='zipcode', max_length=32, widget=forms.TextInput(attrs={'class' : 'form-control'}))

class InvoiceForm(forms.Form):
    subtotal = forms.FloatField(label='subtotal', disabled=True, initial=0, widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    tax = forms.FloatField(label='tax', disabled=True, initial=0, widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    total = forms.FloatField(label='total', disabled=True, initial=0, widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    terms = forms.CharField(label='terms', widget=forms.Textarea(attrs={'class' : 'form-control'}))

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user','')
        super(InvoiceForm, self).__init__(*args, **kwargs)
        self.fields['client']=forms.ModelChoiceField(queryset=Client.objects.filter(owner=user), label='client', widget=forms.Select(attrs={'class' : 'form-control'}))

class InvoiceItemForm(forms.Form):
    item = forms.ModelChoiceField(queryset=Item.objects.all(), label='item', widget=forms.Select(attrs={'class' : 'form-control'}))
    amount = forms.IntegerField(label='amount', widget=forms.NumberInput(attrs={'class' : 'form-control'}))
    qty_rate = forms.IntegerField(label='qty_rate', widget=forms.NumberInput(attrs={'class' : 'form-control'}))
