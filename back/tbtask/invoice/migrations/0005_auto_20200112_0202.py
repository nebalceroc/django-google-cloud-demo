# Generated by Django 3.0.2 on 2020-01-12 02:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invoice', '0004_invoice_owner'),
    ]

    operations = [
        migrations.AlterField(
            model_name='invoice',
            name='subtotal',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='tax',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='invoice',
            name='total',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='item',
            name='price',
            field=models.FloatField(default=0),
        ),
    ]
