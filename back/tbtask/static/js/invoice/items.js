$(document).ready(function(){

    function loadItems() {
        $.ajax({
            url : '/api/items/',
            type : 'GET',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            dataType:'json',
            success : function(data) {
                let item_list = '';
                for (const item of data) {
                    const id = item.id;
                    const name = item.name;
                    const price = item.price
                    let item_element = `
                        <div id="item-${id}" class="feed-element">
                            <div class="media-body ">
                                <small class="float-right">${price}</small>
                                <strong>${name}<br>
                            </div>
                        </div>`;
                    item_list = item_list.concat(item_element);
                }
                $('#item-list').html(item_list);
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    loadItems();
});
