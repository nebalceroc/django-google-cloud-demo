$(document).ready(function(){

    function saveProfile() {
        $.ajax({
            url : '/api/user_profiles/'+$('#id_id').val()+'/',
            type : 'PATCH',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            data: {
                'phone': $('#id_phone').val(),
                'address1': $('#id_address1').val(),
                'address2': $('#id_address2').val(),
                'zipcode': $('#id_zipcode').val()
            },
            dataType:'json',
            success : function(data) {
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });

        $.ajax({
            url : '/api/users/'+user_id+'/',
            type : 'PATCH',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            data: {
                'first_name': $('#id_first_name').val(),
                'last_name': $('#id_last_name').val(),
                'email': $('#id_email').val(),
            },
            dataType:'json',
            success : function(data) {
                $('#modal-title').html('');
                $('#modal-content').html('');
                $('#modal-submit-btn').html('');
                $('#modal').modal('hide');
                $('#user-profile-btn').unbind();
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });



    }

    $('#profile-a').click(function() {
        $.ajax({
            url : '/forms/profile/',
            type : 'GET',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            dataType:'html',
            success : function(data) {
                $('#modal-title').html('User profile');
                $('#modal-content').html(data);
                $('#user-profile-btn').html('Save');
                $('#modal').modal('show');
                $('#user-profile-form').submit(function(event){
                    // cancels the form submission
                    event.preventDefault();
                    saveProfile();
                });
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    });
});
