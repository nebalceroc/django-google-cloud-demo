$(document).ready(function(){

    function createInvoice() {
        $.ajax({
            url : '/api/invoices/',
            type : 'POST',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            data: {
                'owner': `${user_id}`,
                'first_name': $('#id_first_name').val(),
                'last_name': $('#id_last_name').val(),
                'address1': $('#id_address1').val(),
                'address2': $('#id_address2').val(),
                'zipcode': $('#id_zipcode').val()
            },
            dataType:'json',
            success : function(data) {
                $('#modal-title').html('');
                $('#modal-content').html('');
                $('#modal-submit-btn').html('');
                $('#modal').modal('hide');
                $('#modal-submit-btn').unbind();
                loadClients();
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    function deleteInvoice(id) {
        $.ajax({
            url : '/api/invoices/'+id+'/',
            type : 'DELETE',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            data: {
                'id': id
            },
            dataType:'json',
            success : function(data) {
                loadInvoices();
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    function loadInvoices() {
        $.ajax({
            url : '/api/invoices/user_invoices/',
            type : 'GET',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            dataType:'json',
            success : function(data) {
                let invoice_list = '';
                for (const invoice of data) {
                    console.log(invoice);
                    const id = invoice.id;
                    const client = invoice.client.first_name + " " + invoice.client.last_name;
                    const subtotal = invoice.subtotal;
                    const tax = invoice.tax;
                    const total = invoice.total;
                    const issue_date = invoice.issue_date;
                    const terms = invoice.terms;
                    let invoice_element = `
                        <div id="invoice-${id}" class="feed-element">
                            <div class="media-body ">
                                <small class="float-right">$${total}</small>
                                <strong>${client}<br>
                                <small class="text-muted">${issue_date}</small>
                                <div class="actions">
                                    <a class="btn btn-xs btn-white" href="/invoice/${id}/"><i class="fa fa-info"></i> View invoice </a>
                                    <a id="${id}" class="btn btn-xs btn-white edit-btn"><i class="fa fa-pencil"></i> Edit invoice </a>
                                    <a id="${id}" class="btn btn-xs btn-white delete-btn"><i class="fa fa-trash"></i> Delete invoice </a>
                                </div>
                            </div>
                        </div>`;
                    invoice_list = invoice_list.concat(invoice_element);
                }
                $('#invoice-list').html(invoice_list);
                $('.edit-btn').click(function() {
                    window.location.replace('/edit/'+this.id+'/');
                });

                $('.delete-btn').click(function() {
                    deleteInvoice(this.id);
                });
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    function getEmptyInvoiceForm() {
        $.ajax({
            url : '/forms/invoice/',
            type : 'GET',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            dataType:'html',
            success : function(data) {
                $('#modal-title').html('Invoice form');
                $('#modal-content').html(data);
                $('#modal-submit-btn').html('Create');
                $('#modal').modal('show');
                $('#modal-form').submit(function(event){
                    // cancels the form submission
                    event.preventDefault();
                    createInvoice();
                });
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    $('#create-invoice-btn').click(function() {
        window.location.replace('/create');
    });

    loadInvoices();
});
