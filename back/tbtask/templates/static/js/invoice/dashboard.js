$(document).ready(function(){
    function loadRecentInvoices() {
        $.ajax({
            url : '/api/invoices/recent_invoices/',
            type : 'GET',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            dataType:'json',
            success : function(data) {
                let invoice_list = '';
                for (const invoice of data) {
                    const id = invoice.id;
                    const client = invoice.client.first_name + ' ' + invoice.client.last_name
                    const issue_date = invoice.issue_date;
                    const total = invoice.total;
                    let invoice_element = `
                        <li class="list-group-item fist-item">
                            <span class="float-right">
                                ${issue_date}
                            </span>
                            <span class="label label-default">$${total}</span> ${client}
                        </li>`;
                    invoice_list = invoice_list.concat(invoice_element);
                }
                $('#invoice-list').html(invoice_list);
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    loadRecentInvoices();
});
