$(document).ready(function(){

    let clients = [];

    function saveClient(id) {
        $.ajax({
            url : '/api/clients/'+id+'/',
            type : 'PATCH',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            data: {
                'owner': `${user_id}`,
                'phone': $('#id_phone').val(),
                'first_name': $('#id_first_name').val(),
                'last_name': $('#id_last_name').val(),
                'address1': $('#id_address1').val(),
                'address2': $('#id_address2').val(),
                'zipcode': $('#id_zipcode').val()
            },
            dataType:'json',
            success : function(data) {
                $('#modal-title').html('');
                $('#modal-content').html('');
                $('#modal-submit-btn').html('');
                $('#modal').modal('hide');
                $('#modal-submit-btn').unbind();
                loadClients();
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    function createClient(id) {
        $.ajax({
            url : '/api/clients/',
            type : 'POST',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            data: {
                'owner': `${user_id}`,
                'phone': $('#id_phone').val(),
                'first_name': $('#id_first_name').val(),
                'last_name': $('#id_last_name').val(),
                'address1': $('#id_address1').val(),
                'address2': $('#id_address2').val(),
                'zipcode': $('#id_zipcode').val()
            },
            dataType:'json',
            success : function(data) {
                $('#modal-title').html('');
                $('#modal-content').html('');
                $('#modal-submit-btn').html('');
                $('#modal').modal('hide');
                $('#modal-submit-btn').unbind();
                loadClients();
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    function deleteClient(id) {
        $.ajax({
            url : '/api/clients/'+id+'/',
            type : 'DELETE',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            data: {
                'id': id
            },
            dataType:'json',
            success : function(data) {
                loadClients();
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    function loadClients() {
        clients = {};
        $.ajax({
            url : '/api/clients/user_clients',
            type : 'GET',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            dataType:'json',
            success : function(data) {
                let client_list = '';
                for (const client of data) {
                    clients[client.id] = client;
                    const id = client.id;
                    const name = client.first_name + " " + client.last_name;
                    const address = client.address1 + " " + client.address2;
                    const zipcode = client.zipcode;
                    let client_element = `
                        <div id="client-${id}" class="feed-element">
                            <div class="media-body ">
                                <small class="float-right">${zipcode}</small>
                                <strong>${name}<br>
                                <small class="text-muted">${address}</small>
                                <div class="actions">
                                    <a id="${id}" class="btn btn-xs btn-white edit-btn"><i class="fa fa-pencil"></i> Edit client </a>
                                    <a id="${id}" class="btn btn-xs btn-white delete-btn"><i class="fa fa-trash"></i> Delete client </a>
                                </div>
                            </div>
                        </div>`;
                    client_list = client_list.concat(client_element);
                }
                $('#client-list').html(client_list);
                $('.edit-btn').click(function() {
                    getClientForm(this.id);
                });

                $('.delete-btn').click(function() {
                    deleteClient(this.id);
                });
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }



    function getEmptyClientForm() {
        $.ajax({
            url : '/forms/client/',
            type : 'GET',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            dataType:'html',
            success : function(data) {
                $('#modal-title').html('Client form');
                $('#modal-content').html(data);
                $('#modal-submit-btn').html('Create');
                $('#modal').modal('show');
                $('#modal-form').submit(function(event){
                    // cancels the form submission
                    event.preventDefault();
                    createClient();
                });
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    function getClientForm(id) {
        $.ajax({
            url : '/forms/client/',
            type : 'GET',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            dataType:'html',
            success : function(data) {
                $('#modal-title').html('Client form');
                $('#modal-content').html(data);
                $('#modal-submit-btn').html('Save');
                $('#modal').modal('show');
                $('#modal-form').submit(function(event){
                    // cancels the form submission
                    event.preventDefault();
                    saveClient(id);
                });
                $('#id_first_name').val(clients[id].first_name);
                $('#id_last_name').val(clients[id].last_name);
                $('#id_phone').val(clients[id].phone);
                $('#id_address1').val(clients[id].address1);
                $('#id_address2').val(clients[id].address2);
                $('#id_zipcode').val(clients[id].zipcode);
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    $('#add-client-btn').click(function() {
        getEmptyClientForm();
    });

    loadClients();
});
