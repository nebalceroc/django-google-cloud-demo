$(document).ready(function(){

    function loadInvoice() {
        $.ajax({
            url : '/api/invoices/'+invoice_id+'/',
            type : 'GET',
            headers: {
                'Authorization':`Token ${drf_token}`,
            },
            dataType:'json',
            success : function(invoice) {
                let owner_address = `
                <strong>${invoice.owner.user.first_name + ' ' + invoice.owner.user.last_name}<br></strong><br>
                ${invoice.owner.address1}<br><br>
                ${invoice.owner.address2}<br> ${invoice.owner.zipcode}<br><br>
                ${invoice.owner.phone}<br>`;
                $('#owner-address').html(owner_address);
                let client_address = `
                <strong>${invoice.client.first_name + ' ' + invoice.client.last_name}<br></strong><br>
                ${invoice.client.address1}<br><br>
                ${invoice.client.address2}<br> ${invoice.client.zipcode}<br><br>
                ${invoice.client.phone}<br>`;
                $('#client_address').html(client_address);
                $('#invoice-id').text(invoice.id);
                $('#issue-date').text(invoice.issue_date);
                $('#subtotal').text('$'+invoice.subtotal);
                $('#tax').text('$'+invoice.tax);
                $('#total').text('$'+invoice.total);
                $('#terms').text(invoice.terms);
                let item_list = '';
                for (const item of invoice.items) {
                    let total = parseFloat(item.amount) * parseFloat(item.item.price);
                    let item_element = `
                    <tr>
                        <td><div><strong>${item.item.name}</strong></div>
                            <small>${item.item.description}</small></td>
                        <td>${item.amount}</td>
                        <td>$${item.item.price}</td>
                        <td>$${total}</td>
                    </tr>`;
                    item_list = item_list.concat(item_element);
                }
                $('#item-table').html(item_list);
            },
            error : function(request,error)
            {
                console.log('Request: '+request);
                console.log('Error: '+error);
            }
        });
    }

    $('#pdf-btn').click(function() {
        window.location='/pdf/'+invoice_id+'/';
    });

    loadInvoice();
});
