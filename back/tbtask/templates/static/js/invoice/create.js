$(document).ready(function(){

    let item_list = [];
    let item_list_i = 0;
    let subtotal = 0;
    let tax = 19;
    let tax_amount = 0;
    let total = 0;

    function createInvoice() {
        let item_array = [];
        for (const invoice_item of item_list) {
            item_array.push({
                "id": -1,
                "item": invoice_item.item.id,
                "amount": invoice_item.amount,
                "qty_rate": invoice_item.qty_rate
            });
        }
        $.ajax({
            url : "/api/create_invoice/",
            type : "POST",
            headers: {
                "Authorization":`Token ${drf_token}`,
            },
            data: {
                "owner": `${user_id}`,
                "client": $("#id_client").val(),
                "subtotal": $("#id_subtotal").val(),
                "tax": $("#id_tax").val(),
                "total": $("#id_total").val(),
                "terms": $("#id_terms").val(),
                "items": JSON.stringify(item_array)
            },
            success : function(invoice) {
                window.location.replace("/invoice/"+invoice.id+"/");
            },
            error : function(request,error)
            {
                console.log(request);
                console.log("Error: "+error);
            }
        });
    }

    function saveInvoice(id) {
        let item_array = [];
        for (const invoice_item of item_list) {
            console.log(invoice_item);
            let arr_id = -1;
            if (invoice_item.id) {
                arr_id = invoice_item.id;
            }
            item_array.push({
                "id": arr_id,
                "item": invoice_item.item.id,
                "amount": invoice_item.amount,
                "qty_rate": invoice_item.qty_rate
            });
        }
        $.ajax({
            url : "/api/create_invoice/"+id+"/",
            type : "PATCH",
            headers: {
                "Authorization":`Token ${drf_token}`,
            },
            data: {
                "owner": `${user_id}`,
                "client": $("#id_client").val(),
                "subtotal": $("#id_subtotal").val(),
                "tax": $("#id_tax").val(),
                "total": $("#id_total").val(),
                "terms": $("#id_terms").val(),
                "items": JSON.stringify(item_array)
            },
            success : function(invoice) {
            },
            error : function(request,error)
            {
                console.log(request);
                console.log("Error: "+error);
            }
        });
    }

    function forceInvoiceUpdate(id) {
        if (typeof invoice_id !== 'undefined') {
            saveInvoice(invoice_id);
        }
    }

    function deleteInvoiceItem(i) {
        for (const item of item_list) {
            if (item.i == i) {
                calcPrice(-(item.amount * item.price));
                const index = item_list.indexOf(item);
                    if (index > -1) {
                    item_list.splice(index, 1);
                }
                if (item.id != -1 && item.id != undefined) {
                    console.log(item);
                    $.ajax({
                        url : '/api/invoice_items/'+item.id+'/',
                        type : 'DELETE',
                        headers: {
                            'Authorization':`Token ${drf_token}`,
                        },
                        data: {
                            'id': item.id
                        },
                        dataType:'json',
                        success : function(data) {
                            reloadItemList();
                            forceInvoiceUpdate();
                        },
                        error : function(request,error)
                        {
                            console.log('Request: '+request);
                            console.log('Error: '+error);
                        }
                    });
                } else {
                    reloadItemList();
                }
                break;
            }
        }


    }

    function reloadItemList() {
        let item_list_html = "";
        for (const invoice_item of item_list) {
            const item_str = invoice_item.item_str;
            const amount = invoice_item.amount;
            const qty_rate = invoice_item.qty_rate;
            let item_list_element_html = `
                <tr>
                    <td>${item_str}</td>
                    <td class="text-center small">${qty_rate}</td>
                    <td class="text-center"><span class="label label-primary">${amount}</span></td>
                    <td id="${invoice_item.i}" class="delete-td"><i class="fa fa-trash"></i></td>
                </tr>`;
            item_list_html = item_list_html.concat(item_list_element_html);
        }
        $("#create-items-content").html(item_list_html);

        $('.delete-td').click(function() {
            deleteInvoiceItem(this.id);
        });
    }

    function calcPrice(new_price) {
        subtotal = subtotal + new_price;
        tax_amount = subtotal * tax / 100;
        total = subtotal + tax_amount;
        $("#id_subtotal").val(subtotal);
        $("#id_tax").val(tax_amount);
        $("#id_total").val(total);
    }

    function addItem() {
        let item_id = $("#id_item").val();
        let item_str = $("#id_item option:selected").text();
        $.ajax({
            url : "/api/items/"+item_id+"/",
            type : "GET",
            headers: {
                "Authorization":`Token ${drf_token}`,
            },
            dataType:"json",
            success : function(item) {
                let amount = $("#id_amount").val();
                let qty_rate = $("#id_qty_rate").val();
                let price = item.price;
                item_list.push({
                    "i": item_list_i,
                    "item": item,
                    "item_str": item_str,
                    "amount": amount,
                    "qty_rate": qty_rate,
                    "price": price,
                });
                item_list_i++;
                calcPrice(price * amount);
                $("#modal-title").html("");
                $("#modal-content").html("");
                $("#modal").modal("hide");
                $("#invoice-item-form").unbind();
                $("#invoice-item-btn").unbind();
                reloadItemList();
            },
            error : function(request,error)
            {
                console.log("Request: "+request);
                console.log("Error: "+error);
            }
        });
    }

    function getInvoice(id) {
        $.ajax({
            url : "/api/invoices/"+id+"/",
            type : "GET",
            headers: {
                "Authorization":`Token ${drf_token}`,
            },
            dataType:"json",
            success : function(invoice) {
                $("#modal-submit-btn").html("Save invoice");
                $("#modal-form").submit(function(event){
                    // cancels the form submission
                    event.preventDefault();
                    saveInvoice(id);
                });
                subtotal = invoice.subtotal;
                tax_amount = invoice.tax;
                total = invoice.total;
                $("#id_client").val(invoice.client.id);
                $("#id_subtotal").val(subtotal);
                $("#id_tax").val(tax_amount);
                $("#id_total").val(total);
                $("#id_terms").val(invoice.terms);
                for (const item of invoice.items) {
                    console.log(item);
                    const amount = item.amount;
                    const qty_rate = item.qty_rate;
                    const price = item.item.price;
                    const item_str = `${item.item.name} $${price.toFixed(1)} u`;
                    item_list.push({
                        "i": item_list_i,
                        "id": item.id,
                        "item": item.item,
                        "item_str": item_str,
                        "amount": amount,
                        "qty_rate": qty_rate,
                        "price": price,
                    });
                    item_list_i++;
                }
                reloadItemList();
            },
            error : function(request,error)
            {
                window.location.replace("/main/invoices/");
                console.log("Request: "+request);
                console.log("Error: "+error);
            }
        });
    }

    function getEmptyInvoiceForm() {
        $.ajax({
            url : "/forms/invoice/",
            type : "GET",
            headers: {
                "Authorization":`Token ${drf_token}`,
            },
            dataType:"html",
            success : function(data) {
                $("#create-form-content").html(data);
                $("#modal-submit-btn").html("Create invoice");
                if (typeof invoice_id !== 'undefined') {
                    getInvoice(invoice_id);
                } else {
                    $("#modal-form").submit(function(event){
                        // cancels the form submission
                        event.preventDefault();
                        createInvoice();
                    });
                }
            },
            error : function(request,error)
            {
                console.log("Request: "+request);
                console.log("Error: "+error);
            }
        });
    }

    function getEmptyInvoiceItemForm() {
        $.ajax({
            url : "/forms/invoice_item/",
            type : "GET",
            headers: {
                "Authorization":`Token ${drf_token}`,
            },
            dataType:"html",
            success : function(data) {
                $("#modal-title").html("Add item");
                $("#modal-content").html(data);
                $("#invoice-item-btn").html("Add");
                $("#modal").modal("show");
                $("#invoice-item-form").submit(function(event){
                    // cancels the form submission
                    event.preventDefault();
                    addItem();
                });
            },
            error : function(request,error)
            {
                console.log("Request: "+request);
                console.log("Error: "+error);
            }
        });
    }

    $("#add-item-btn").click(function() {
        getEmptyInvoiceItemForm();
    });

    getEmptyInvoiceForm();
});
