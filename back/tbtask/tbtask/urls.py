"""tbtask URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from invoice.views import *

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'user_profiles', UserProfileViewSet)
router.register(r'clients', ClientViewSet)
router.register(r'items', ItemViewSet)
router.register(r'invoices', InvoiceViewSet)
router.register(r'create_invoice', CreateInvoiceViewSet)
router.register(r'invoice_items', InvoiceItemViewSet)

urlpatterns = [
    path('', IndexView.as_view()),
    path('create/', CreateView.as_view()),
    path('edit/<int:id>/', EditView.as_view()),
    path('invoice/<int:id>/', InvoiceView.as_view()),
    path('main/<str:module>/', MainView.as_view()),
    path('forms/<str:form>/', FormsView.as_view()),
    path('pdf/<int:invoice_id>/', PdfView.as_view()),
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    #path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
